﻿namespace _01___Calcul_du_Salaire
{
    partial class FrmSalaire
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalaire));
            this.lblHeures = new System.Windows.Forms.Label();
            this.numHeures = new System.Windows.Forms.NumericUpDown();
            this.lblSalaireHoraire = new System.Windows.Forms.Label();
            this.lblDebut = new System.Windows.Forms.Label();
            this.dateDebut = new System.Windows.Forms.DateTimePicker();
            this.grpPeriode = new System.Windows.Forms.GroupBox();
            this.dateFin = new System.Windows.Forms.DateTimePicker();
            this.lblFin = new System.Windows.Forms.Label();
            this.grpParamLegaux = new System.Windows.Forms.GroupBox();
            this.lblDuree = new System.Windows.Forms.Label();
            this.numDuree = new System.Windows.Forms.NumericUpDown();
            this.lblHeureSemaine = new System.Windows.Forms.Label();
            this.grpParamSalarie = new System.Windows.Forms.GroupBox();
            this.lblEuroHeure1 = new System.Windows.Forms.Label();
            this.numSMIC = new System.Windows.Forms.NumericUpDown();
            this.lblSMIC = new System.Windows.Forms.Label();
            this.lblEuroHeure2 = new System.Windows.Forms.Label();
            this.numSalaireHoraire = new System.Windows.Forms.NumericUpDown();
            this.lblEuroHeure3 = new System.Windows.Forms.Label();
            this.numSalaireMajore = new System.Windows.Forms.NumericUpDown();
            this.lblSalaireMajore = new System.Windows.Forms.Label();
            this.lblHeureMois = new System.Windows.Forms.Label();
            this.lblSalaire = new System.Windows.Forms.Label();
            this.numSalaire = new System.Windows.Forms.NumericUpDown();
            this.lblEuro = new System.Windows.Forms.Label();
            this.btnCalculer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numHeures)).BeginInit();
            this.grpPeriode.SuspendLayout();
            this.grpParamLegaux.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDuree)).BeginInit();
            this.grpParamSalarie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSMIC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalaireHoraire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalaireMajore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalaire)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeures
            // 
            this.lblHeures.AutoSize = true;
            this.lblHeures.Location = new System.Drawing.Point(267, 110);
            this.lblHeures.Name = "lblHeures";
            this.lblHeures.Size = new System.Drawing.Size(101, 13);
            this.lblHeures.TabIndex = 0;
            this.lblHeures.Text = "Heures Travaillées :";
            // 
            // numHeures
            // 
            this.numHeures.DecimalPlaces = 2;
            this.numHeures.Location = new System.Drawing.Point(374, 108);
            this.numHeures.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numHeures.Name = "numHeures";
            this.numHeures.Size = new System.Drawing.Size(56, 20);
            this.numHeures.TabIndex = 1;
            this.numHeures.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSalaireHoraire
            // 
            this.lblSalaireHoraire.AutoSize = true;
            this.lblSalaireHoraire.Location = new System.Drawing.Point(6, 21);
            this.lblSalaireHoraire.Name = "lblSalaireHoraire";
            this.lblSalaireHoraire.Size = new System.Drawing.Size(80, 13);
            this.lblSalaireHoraire.TabIndex = 2;
            this.lblSalaireHoraire.Text = "Salaire horaire :";
            // 
            // lblDebut
            // 
            this.lblDebut.AutoSize = true;
            this.lblDebut.Location = new System.Drawing.Point(6, 21);
            this.lblDebut.Name = "lblDebut";
            this.lblDebut.Size = new System.Drawing.Size(25, 13);
            this.lblDebut.TabIndex = 4;
            this.lblDebut.Text = "du :";
            // 
            // dateDebut
            // 
            this.dateDebut.Location = new System.Drawing.Point(37, 19);
            this.dateDebut.Name = "dateDebut";
            this.dateDebut.Size = new System.Drawing.Size(200, 20);
            this.dateDebut.TabIndex = 5;
            this.dateDebut.Value = new System.DateTime(2018, 9, 1, 21, 29, 0, 0);
            // 
            // grpPeriode
            // 
            this.grpPeriode.Controls.Add(this.dateFin);
            this.grpPeriode.Controls.Add(this.lblFin);
            this.grpPeriode.Controls.Add(this.dateDebut);
            this.grpPeriode.Controls.Add(this.lblDebut);
            this.grpPeriode.Location = new System.Drawing.Point(261, 12);
            this.grpPeriode.Name = "grpPeriode";
            this.grpPeriode.Size = new System.Drawing.Size(243, 71);
            this.grpPeriode.TabIndex = 6;
            this.grpPeriode.TabStop = false;
            this.grpPeriode.Text = "Période";
            // 
            // dateFin
            // 
            this.dateFin.Location = new System.Drawing.Point(37, 45);
            this.dateFin.Name = "dateFin";
            this.dateFin.Size = new System.Drawing.Size(200, 20);
            this.dateFin.TabIndex = 7;
            this.dateFin.Value = new System.DateTime(2018, 9, 30, 21, 30, 0, 0);
            // 
            // lblFin
            // 
            this.lblFin.AutoSize = true;
            this.lblFin.Location = new System.Drawing.Point(6, 48);
            this.lblFin.Name = "lblFin";
            this.lblFin.Size = new System.Drawing.Size(25, 13);
            this.lblFin.TabIndex = 6;
            this.lblFin.Text = "au :";
            // 
            // grpParamLegaux
            // 
            this.grpParamLegaux.Controls.Add(this.lblEuroHeure1);
            this.grpParamLegaux.Controls.Add(this.lblHeureSemaine);
            this.grpParamLegaux.Controls.Add(this.numSMIC);
            this.grpParamLegaux.Controls.Add(this.lblSMIC);
            this.grpParamLegaux.Controls.Add(this.numDuree);
            this.grpParamLegaux.Controls.Add(this.lblDuree);
            this.grpParamLegaux.Location = new System.Drawing.Point(12, 12);
            this.grpParamLegaux.Name = "grpParamLegaux";
            this.grpParamLegaux.Size = new System.Drawing.Size(243, 71);
            this.grpParamLegaux.TabIndex = 7;
            this.grpParamLegaux.TabStop = false;
            this.grpParamLegaux.Text = "Paramètres Légaux";
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.Location = new System.Drawing.Point(6, 21);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(42, 13);
            this.lblDuree.TabIndex = 0;
            this.lblDuree.Text = "Durée :";
            // 
            // numDuree
            // 
            this.numDuree.Location = new System.Drawing.Point(54, 19);
            this.numDuree.Name = "numDuree";
            this.numDuree.Size = new System.Drawing.Size(43, 20);
            this.numDuree.TabIndex = 8;
            this.numDuree.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numDuree.Value = new decimal(new int[] {
            35,
            0,
            0,
            0});
            // 
            // lblHeureSemaine
            // 
            this.lblHeureSemaine.AutoSize = true;
            this.lblHeureSemaine.Location = new System.Drawing.Point(103, 21);
            this.lblHeureSemaine.Name = "lblHeureSemaine";
            this.lblHeureSemaine.Size = new System.Drawing.Size(43, 13);
            this.lblHeureSemaine.TabIndex = 8;
            this.lblHeureSemaine.Text = "h / sem";
            // 
            // grpParamSalarie
            // 
            this.grpParamSalarie.Controls.Add(this.lblEuroHeure3);
            this.grpParamSalarie.Controls.Add(this.numSalaireMajore);
            this.grpParamSalarie.Controls.Add(this.lblSalaireMajore);
            this.grpParamSalarie.Controls.Add(this.lblEuroHeure2);
            this.grpParamSalarie.Controls.Add(this.numSalaireHoraire);
            this.grpParamSalarie.Controls.Add(this.lblSalaireHoraire);
            this.grpParamSalarie.Location = new System.Drawing.Point(12, 89);
            this.grpParamSalarie.Name = "grpParamSalarie";
            this.grpParamSalarie.Size = new System.Drawing.Size(243, 71);
            this.grpParamSalarie.TabIndex = 9;
            this.grpParamSalarie.TabStop = false;
            this.grpParamSalarie.Text = "Paramètres Salarié";
            // 
            // lblEuroHeure1
            // 
            this.lblEuroHeure1.AutoSize = true;
            this.lblEuroHeure1.Location = new System.Drawing.Point(116, 47);
            this.lblEuroHeure1.Name = "lblEuroHeure1";
            this.lblEuroHeure1.Size = new System.Drawing.Size(30, 13);
            this.lblEuroHeure1.TabIndex = 11;
            this.lblEuroHeure1.Text = "€ / h";
            // 
            // numSMIC
            // 
            this.numSMIC.DecimalPlaces = 2;
            this.numSMIC.Location = new System.Drawing.Point(54, 45);
            this.numSMIC.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.numSMIC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSMIC.Name = "numSMIC";
            this.numSMIC.Size = new System.Drawing.Size(56, 20);
            this.numSMIC.TabIndex = 10;
            this.numSMIC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSMIC.Value = new decimal(new int[] {
            988,
            0,
            0,
            131072});
            this.numSMIC.ValueChanged += new System.EventHandler(this.numSMIC_ValueChanged);
            // 
            // lblSMIC
            // 
            this.lblSMIC.AutoSize = true;
            this.lblSMIC.Location = new System.Drawing.Point(6, 47);
            this.lblSMIC.Name = "lblSMIC";
            this.lblSMIC.Size = new System.Drawing.Size(39, 13);
            this.lblSMIC.TabIndex = 9;
            this.lblSMIC.Text = "SMIC :";
            // 
            // lblEuroHeure2
            // 
            this.lblEuroHeure2.AutoSize = true;
            this.lblEuroHeure2.Location = new System.Drawing.Point(154, 21);
            this.lblEuroHeure2.Name = "lblEuroHeure2";
            this.lblEuroHeure2.Size = new System.Drawing.Size(30, 13);
            this.lblEuroHeure2.TabIndex = 13;
            this.lblEuroHeure2.Text = "€ / h";
            // 
            // numSalaireHoraire
            // 
            this.numSalaireHoraire.DecimalPlaces = 2;
            this.numSalaireHoraire.Location = new System.Drawing.Point(92, 19);
            this.numSalaireHoraire.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.numSalaireHoraire.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSalaireHoraire.Name = "numSalaireHoraire";
            this.numSalaireHoraire.Size = new System.Drawing.Size(56, 20);
            this.numSalaireHoraire.TabIndex = 12;
            this.numSalaireHoraire.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSalaireHoraire.Value = new decimal(new int[] {
            988,
            0,
            0,
            131072});
            this.numSalaireHoraire.ValueChanged += new System.EventHandler(this.numSalaireHoraire_ValueChanged);
            // 
            // lblEuroHeure3
            // 
            this.lblEuroHeure3.AutoSize = true;
            this.lblEuroHeure3.Location = new System.Drawing.Point(154, 47);
            this.lblEuroHeure3.Name = "lblEuroHeure3";
            this.lblEuroHeure3.Size = new System.Drawing.Size(30, 13);
            this.lblEuroHeure3.TabIndex = 16;
            this.lblEuroHeure3.Text = "€ / h";
            // 
            // numSalaireMajore
            // 
            this.numSalaireMajore.DecimalPlaces = 2;
            this.numSalaireMajore.Location = new System.Drawing.Point(92, 45);
            this.numSalaireMajore.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.numSalaireMajore.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSalaireMajore.Name = "numSalaireMajore";
            this.numSalaireMajore.Size = new System.Drawing.Size(56, 20);
            this.numSalaireMajore.TabIndex = 15;
            this.numSalaireMajore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSalaireMajore.Value = new decimal(new int[] {
            1088,
            0,
            0,
            131072});
            this.numSalaireMajore.ValueChanged += new System.EventHandler(this.numSalaireMajore_ValueChanged);
            // 
            // lblSalaireMajore
            // 
            this.lblSalaireMajore.AutoSize = true;
            this.lblSalaireMajore.Location = new System.Drawing.Point(6, 47);
            this.lblSalaireMajore.Name = "lblSalaireMajore";
            this.lblSalaireMajore.Size = new System.Drawing.Size(79, 13);
            this.lblSalaireMajore.TabIndex = 14;
            this.lblSalaireMajore.Text = "Salaire majoré :";
            // 
            // lblHeureMois
            // 
            this.lblHeureMois.AutoSize = true;
            this.lblHeureMois.Location = new System.Drawing.Point(436, 110);
            this.lblHeureMois.Name = "lblHeureMois";
            this.lblHeureMois.Size = new System.Drawing.Size(45, 13);
            this.lblHeureMois.TabIndex = 10;
            this.lblHeureMois.Text = "h / mois";
            // 
            // lblSalaire
            // 
            this.lblSalaire.AutoSize = true;
            this.lblSalaire.Location = new System.Drawing.Point(267, 136);
            this.lblSalaire.Name = "lblSalaire";
            this.lblSalaire.Size = new System.Drawing.Size(66, 13);
            this.lblSalaire.TabIndex = 11;
            this.lblSalaire.Text = "Salaire brut :";
            // 
            // numSalaire
            // 
            this.numSalaire.DecimalPlaces = 2;
            this.numSalaire.Location = new System.Drawing.Point(359, 134);
            this.numSalaire.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numSalaire.Name = "numSalaire";
            this.numSalaire.ReadOnly = true;
            this.numSalaire.Size = new System.Drawing.Size(71, 20);
            this.numSalaire.TabIndex = 12;
            this.numSalaire.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSalaire.ThousandsSeparator = true;
            // 
            // lblEuro
            // 
            this.lblEuro.AutoSize = true;
            this.lblEuro.Location = new System.Drawing.Point(436, 136);
            this.lblEuro.Name = "lblEuro";
            this.lblEuro.Size = new System.Drawing.Size(13, 13);
            this.lblEuro.TabIndex = 17;
            this.lblEuro.Text = "€";
            // 
            // btnCalculer
            // 
            this.btnCalculer.Location = new System.Drawing.Point(455, 131);
            this.btnCalculer.Name = "btnCalculer";
            this.btnCalculer.Size = new System.Drawing.Size(75, 23);
            this.btnCalculer.TabIndex = 18;
            this.btnCalculer.Text = "Calculer";
            this.btnCalculer.UseVisualStyleBackColor = true;
            this.btnCalculer.Click += new System.EventHandler(this.btnCalculer_Click);
            // 
            // FrmSalaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(547, 185);
            this.Controls.Add(this.btnCalculer);
            this.Controls.Add(this.lblEuro);
            this.Controls.Add(this.numSalaire);
            this.Controls.Add(this.lblSalaire);
            this.Controls.Add(this.lblHeureMois);
            this.Controls.Add(this.grpParamSalarie);
            this.Controls.Add(this.grpParamLegaux);
            this.Controls.Add(this.grpPeriode);
            this.Controls.Add(this.numHeures);
            this.Controls.Add(this.lblHeures);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSalaire";
            this.Text = "Salaire";
            ((System.ComponentModel.ISupportInitialize)(this.numHeures)).EndInit();
            this.grpPeriode.ResumeLayout(false);
            this.grpPeriode.PerformLayout();
            this.grpParamLegaux.ResumeLayout(false);
            this.grpParamLegaux.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDuree)).EndInit();
            this.grpParamSalarie.ResumeLayout(false);
            this.grpParamSalarie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSMIC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalaireHoraire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalaireMajore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSalaire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeures;
        private System.Windows.Forms.NumericUpDown numHeures;
        private System.Windows.Forms.Label lblSalaireHoraire;
        private System.Windows.Forms.Label lblDebut;
        private System.Windows.Forms.DateTimePicker dateDebut;
        private System.Windows.Forms.GroupBox grpPeriode;
        private System.Windows.Forms.DateTimePicker dateFin;
        private System.Windows.Forms.Label lblFin;
        private System.Windows.Forms.GroupBox grpParamLegaux;
        private System.Windows.Forms.NumericUpDown numDuree;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.Label lblHeureSemaine;
        private System.Windows.Forms.Label lblEuroHeure1;
        private System.Windows.Forms.NumericUpDown numSMIC;
        private System.Windows.Forms.Label lblSMIC;
        private System.Windows.Forms.GroupBox grpParamSalarie;
        private System.Windows.Forms.Label lblEuroHeure3;
        private System.Windows.Forms.NumericUpDown numSalaireMajore;
        private System.Windows.Forms.Label lblSalaireMajore;
        private System.Windows.Forms.Label lblEuroHeure2;
        private System.Windows.Forms.NumericUpDown numSalaireHoraire;
        private System.Windows.Forms.Label lblHeureMois;
        private System.Windows.Forms.Label lblSalaire;
        private System.Windows.Forms.NumericUpDown numSalaire;
        private System.Windows.Forms.Label lblEuro;
        private System.Windows.Forms.Button btnCalculer;
    }
}

