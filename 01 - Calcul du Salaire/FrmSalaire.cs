﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01___Calcul_du_Salaire
{
    public partial class FrmSalaire : Form
    {
        public FrmSalaire()
        {
            InitializeComponent();
        }

        private void numSalaireHoraire_ValueChanged(object sender, EventArgs e)
        {
            // Todo 02. Vérifier que le salaire est bien au dessus du SMIC
            //          Sinon on lui donne la valeur du SMIC
        }

        private void numSalaireMajore_ValueChanged(object sender, EventArgs e)
        {
            // Todo 03. Vérifier que le salaire majoré est bien supérieur d'au moins 10% au salaire horaire
            //          Sinon on lui donne la valeur du salaire horaire + 10% (en arrondissant à 2 chiffres après la virgule)
        }

        private void numSMIC_ValueChanged(object sender, EventArgs e)
        {
            // Todo 04. Vérifier que le salaire horaire est bien au moins à la valeur du SMIC
            //          Sinon on lui donne la valeur du nouveau SMIC
        }

        private void btnCalculer_Click(object sender, EventArgs e)
        {
            // Todo 05. Faire le calcul du salaire
            //          Attention : si le salarié dépasse 35h par semaine (soit 155h par mois)
            //                      les heures en excédant doivent être payées au taux majoré
        }

    }
}
