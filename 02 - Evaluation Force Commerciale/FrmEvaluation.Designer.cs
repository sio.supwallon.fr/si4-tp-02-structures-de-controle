﻿namespace _02___Evaluation_Force_Commerciale
{
    partial class FrmEvaluation
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEvaluation));
            this.lblChiffreAffaire = new System.Windows.Forms.Label();
            this.numChiffreAffaire = new System.Windows.Forms.NumericUpDown();
            this.lblEuro = new System.Windows.Forms.Label();
            this.btnEvaluer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numChiffreAffaire)).BeginInit();
            this.SuspendLayout();
            // 
            // lblChiffreAffaire
            // 
            this.lblChiffreAffaire.AutoSize = true;
            this.lblChiffreAffaire.Location = new System.Drawing.Point(12, 14);
            this.lblChiffreAffaire.Name = "lblChiffreAffaire";
            this.lblChiffreAffaire.Size = new System.Drawing.Size(84, 13);
            this.lblChiffreAffaire.TabIndex = 0;
            this.lblChiffreAffaire.Text = "Chiffre d\'Affaire :";
            // 
            // numChiffreAffaire
            // 
            this.numChiffreAffaire.DecimalPlaces = 2;
            this.numChiffreAffaire.Location = new System.Drawing.Point(102, 12);
            this.numChiffreAffaire.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numChiffreAffaire.Name = "numChiffreAffaire";
            this.numChiffreAffaire.Size = new System.Drawing.Size(120, 20);
            this.numChiffreAffaire.TabIndex = 1;
            this.numChiffreAffaire.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numChiffreAffaire.ThousandsSeparator = true;
            this.numChiffreAffaire.Value = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            // 
            // lblEuro
            // 
            this.lblEuro.AutoSize = true;
            this.lblEuro.Location = new System.Drawing.Point(228, 14);
            this.lblEuro.Name = "lblEuro";
            this.lblEuro.Size = new System.Drawing.Size(13, 13);
            this.lblEuro.TabIndex = 2;
            this.lblEuro.Text = "€";
            // 
            // btnEvaluer
            // 
            this.btnEvaluer.Location = new System.Drawing.Point(247, 12);
            this.btnEvaluer.Name = "btnEvaluer";
            this.btnEvaluer.Size = new System.Drawing.Size(75, 23);
            this.btnEvaluer.TabIndex = 3;
            this.btnEvaluer.Text = "Evaluer";
            this.btnEvaluer.UseVisualStyleBackColor = true;
            this.btnEvaluer.Click += new System.EventHandler(this.btnEvaluer_Click);
            // 
            // FrmEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 49);
            this.Controls.Add(this.btnEvaluer);
            this.Controls.Add(this.lblEuro);
            this.Controls.Add(this.numChiffreAffaire);
            this.Controls.Add(this.lblChiffreAffaire);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEvaluation";
            this.Text = "Evaluation";
            ((System.ComponentModel.ISupportInitialize)(this.numChiffreAffaire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblChiffreAffaire;
        private System.Windows.Forms.NumericUpDown numChiffreAffaire;
        private System.Windows.Forms.Label lblEuro;
        private System.Windows.Forms.Button btnEvaluer;
    }
}

