﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02___Evaluation_Force_Commerciale
{
    public partial class FrmEvaluation : Form
    {
        public FrmEvaluation()
        {
            InitializeComponent();
        }

        private void btnEvaluer_Click(object sender, EventArgs e)
        {
            // Todo 01. Afficher une évaluation en fonction du chiffre d'affaire annuel généré par le commercial
            //                     CA <   100 000 : Avertissement : Résultats médiocres !
            //          100 000 <= CA <   200 000 : Résultats faibles, il faut être plus agressif !
            //          200 000 <= CA <   500 000 : Résultats corrects, il faut faire mieux.
            //          500 000 <= CA < 1 000 000 : Bons résultats, on peut encore progresser.
            //        1 000 000 <= CA             : Très bons résultats, employé exemplaire

            MessageBox.Show("Votre appréciation.");
        }
    }
}
